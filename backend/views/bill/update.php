<?
use common\models\Bill;

/**
 * @var $this yii\web\View
 * @var Bill $model
 */

?>
<h1>Изменение платежа</h1>

<?= $this->render('_form', ['model' => $model]); ?>