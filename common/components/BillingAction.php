<?php

namespace common\components;

use common\models\Balance;
use common\models\Bill;
use common\models\Cost;
use Exception;

class BillingAction
{
	/**
	 * @param Bill $bill
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function changeBill(Bill $bill)
	{
		switch ($bill->type_id) {
			case Bill::TYPE_POSTPAYMENT:
				$balance = $bill->client->balance;
				$sum = self::calculateBillSum($bill);
				self::changeBalanceSum($balance, $sum);

				break;
			case Bill::TYPE_PREPAYMENT:
				$balance = $bill->client->balance;
				self::changeBalanceSum($balance, $bill->sum);

				break;
			default:
				throw new Exception('Такого типа не существует');
		}

		return true;
	}

	/**
	 * @param Balance $balance
	 * @param float   $sum
	 *
	 * @throws Exception
	 */
	private static function changeBalanceSum(Balance $balance, float $sum)
	{
		$balance->sum += $sum;
		if (!$balance->save()) {
			throw new Exception(print_r($balance->getErrors(), true));
		}
	}

	/**
	 * @param Bill $bill
	 *
	 * @return float
	 */
	private static function calculateBillSum(Bill $bill): float
	{
		$costsSum = Bill::getCostsSumByBillId($bill->id);
		$otherPaymentsSumByPeriod = (int) Bill::find()
			->byPk($bill->id, true)
			->byPeriod($bill->date_from, $bill->date_to)
			->select('SUM(sum)')
			->scalar();

		return $costsSum - ($otherPaymentsSumByPeriod + $bill->sum);
	}

	/**
	 * @param Cost $cost
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function changeCost(Cost $cost)
	{
		$balance = $cost->client->balance;
		self::changeBalanceSum($balance, -$cost->sum);

		return true;
	}
}