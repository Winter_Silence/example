<?php

namespace common\models;

use common\components\AdvancedActiveQuery;

/**
 * This is the ActiveQuery class for [[Bill]].
 *
 * @see Bill
 */
class BillQuery extends AdvancedActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Bill[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Bill|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	/**
	 * @param $clientId
	 *
	 * @return $this
	 */
	public function byClientId($clientId)
	{
		return $this->andWhere(['client_id' => $clientId]);
    }

	/**
	 * @return $this
	 */
	public function withCost()
	{
		return $this->joinWith(['cost'])
			->andWhere(Cost::tableName() . '.date_from >= ' . $this->field('date_from'))
			->andWhere(Cost::tableName() . '.date_to <= ' . $this->field('date_to'));
    }

	/**
	 * @param $dateFrom
	 * @param $dateTo
	 *
	 * @return $this
	 */
	public function byPeriod($dateFrom, $dateTo)
	{
		return $this->andWhere(
			[
				'AND',
				['>=', 'date_from', $dateFrom],
				['<=', 'date_to', $dateTo]
			]
		);
    }

	/**
	 * @param $typeId
	 *
	 * @return $this
	 */
	public function byTypeId($typeId)
	{
		return $this->byField('type_id', $typeId);
    }

	/**
	 * Проверка вхождения платежа в заданный промежуток времени
	 *
	 * @param $dateFrom
	 * @param $dateTo
	 *
	 * @return $this
	 */
    public function checkPeriod($dateFrom, $dateTo)
    {
    	return $this->andWhere(
		    [
			    'OR',
			    [
				    'AND',
				    ['>=', 'date_from', $dateFrom],
				    ['<=', 'date_to', $dateTo]
			    ],
			    [
			    	'AND',
			        "'$dateFrom' >= date_from",
				    "'$dateTo' <= date_to"
			    ]
		    ]
	    );
    }
}
